package Server;

//Imports
import java.io.*;
import java.net.*;
import java.util.*;

/**
 *
 * @author nk.WideView
 */

class ClientHandlerV3 implements Runnable {

    String userID;
    String nutzername;
    String passwordHash;
    String chatID;
    String chatname;
    String nachricht;
    String ts;
    String n_UserID;
    String za_userID;
    String nK_userID;
    String zl_userID;
    String ze_userID;
    String[] mitgliederIDs;
    public String name;
    final DataInputStream dis;
    final DataOutputStream dos;
    Socket s;
    boolean erfolg;
    public boolean istOnline;
    public boolean istLogin;
    java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());

    ClientHandlerV3(Socket s, String name,
                    DataInputStream dis, DataOutputStream dos) {
        this.dis = dis;
        this.dos = dos;
        this.name = name;
        this.s = s;
        this.istOnline = true;
        this.istLogin = false;
    }

    @Override
    public void run() {

        //locale Variablen - müssen Teilweise nach oben verschoben werden, da noch getestet wird
        mysqlVerbindungV3 sql = new mysqlVerbindungV3();
        System.out.println("<" + currentTimestamp + "> Thread gestartet");
        String art;
        String versenden;
        String empfangen;
        String[] mitgliederIDs = new String[8];
        boolean bedignung = true;
        while (bedignung) {
            try {
                empfangen = dis.readUTF();
                System.out.println("<" + currentTimestamp + "> DataInputStream:" + empfangen);

                String[] splitResult = empfangen.split(";");
                int anfragenLaenge = splitResult.length;
                art = splitResult[0];
                switch (art) {
                    case "login":
                        System.out.println("<" + currentTimestamp + "> Login-Anfrage von Client: " + name);
                        erfolg = false;
                        if (this.istLogin) {
                            break;
                        }
                        userID = splitResult[1];
                        passwordHash = splitResult[2];
                        erfolg = sql.loginAnfrageErfolg(erfolg, userID, passwordHash);
                        if (erfolg) {
                            this.istLogin = true;
                            this.name = userID;
                            versenden = erfolg+";"+userID;
                            for (ClientHandlerV3 mc : ServerV3.ar) {
                                if (mc.name.equals(userID)) {
                                    mc.dos.writeUTF(versenden);
                                    System.out.println("<" + currentTimestamp + "> " + versenden + " - versendet an: " + name);
                                    break;
                                }
                            }
                        } else {
                            versenden = String.valueOf(erfolg);
                            for (ClientHandlerV3 mc : ServerV3.ar) {
                                if (mc.name.equals(this.name)) {
                                    mc.dos.writeUTF(versenden);
                                    System.out.println("<" + currentTimestamp + "> " + versenden + " - versendet an:" + name);
                                    break;
                                }
                            }
                        }
                        break;
                    case "neuerAccount":
                        System.out.println("<" + currentTimestamp + "> neuerAccount-Anfrage von Client: " + name);
                        erfolg = false;
                        nutzername = splitResult[1];
                        passwordHash = splitResult[2];
                        erfolg = sql.neuerAccountAnfrageErfolg(erfolg, nutzername, passwordHash);
                        if (erfolg) {
                            n_UserID = sql.neuerAccountAnfrageOutput(n_UserID, nutzername);
                            versenden = erfolg+";"+n_UserID;
                            for (ClientHandlerV3 mc : ServerV3.ar) {
                                if (mc.name.equals(this.name)) {
                                    mc.dos.writeUTF(versenden);
                                    System.out.println("<" + currentTimestamp + "> " + versenden + " - versendet");
                                    break;
                                }
                            }
                        }
                        break;
                    case "loescheAccount": // noch nicht fertig
                        System.out.println("<" + currentTimestamp + "> loeascheAccount-Anfrage von Client: " + name);
                        this.erfolg = false;
                        if (!this.istLogin) {
                            break;
                        }
                        zl_userID = this.name;
                        erfolg = sql.loescheAccountErfolg(erfolg, zl_userID);
                        if (erfolg) {
                            this.name = "client " + ServerV3.clientZaehler;
                            this.istLogin = false;
                            versenden = erfolg+";"+zl_userID;
                            for (ClientHandlerV3 mc : ServerV3.ar) {
                                if (mc.name.equals(this.name)) {
                                    mc.dos.writeUTF(versenden);
                                    System.out.println("<" + currentTimestamp + "> " + versenden + " - versendet");
                                    break;
                                }
                            }
                        }
                        break;
                    case "neueNachricht":
                        System.out.println("<" + currentTimestamp + "> neueNachricht-Anfrage von Client: " + name);
                        this.erfolg = false;
                        if (!this.istLogin) {
                            break;
                        }
                        chatID = splitResult[1];
                        userID = this.name;
                        nachricht = splitResult[2];
                        ts = splitResult[3] + ";" + splitResult[4] + ";" + splitResult[5] + ";" + splitResult[6] + ";" + splitResult[7];
                        erfolg = sql.neueNachrichtAnfrageErfolg(erfolg, chatID, userID, nachricht, ts);
                        if (erfolg) {
                            mitgliederIDs = sql.neueNachrichtAnfrageOutput(mitgliederIDs, chatID);
                            versenden = ts+";"+nachricht+";"+userID;
                            for (String mitgliederID : mitgliederIDs) {
                                for (ClientHandlerV3 mc : ServerV3.ar) {
                                    if (mc.name.equals(mitgliederID) && !mc.name.equals(userID)) {
                                        mc.dos.writeUTF(versenden);
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    case "neuerChat":
                        System.out.println("<" + currentTimestamp + "> neuerChat-Anfrage von Client: " + name);
                        erfolg = false;
                        if (!this.istLogin) {
                            System.out.println("nicht eingeloggt");
                            break;
                        }
                        chatname = splitResult[1];
                        for (int i = 2; i < anfragenLaenge - 5; i++) {
                            mitgliederIDs[i - 2] = splitResult[i];
                        }
                        ts = splitResult[anfragenLaenge - 5] + ";" + splitResult[anfragenLaenge - 4] + ";" + splitResult[anfragenLaenge - 3] + ";" + splitResult[anfragenLaenge - 2] + ";" + splitResult[anfragenLaenge - 1];
                        erfolg = sql.neuerChatAnfrageErfolg(erfolg, chatname, mitgliederIDs);
                        if (erfolg) {
                            chatID = sql.neuerChatAnfrageOutput(chatID, chatname);
                            //nachricht = "neuer Chat erstellt: ChatID: " + chatID + ", Chatname: " + chatname + ", mitgliederIDs: " + mitgliederIDs[0] + mitgliederIDs[1] + mitgliederIDs[2] + mitgliederIDs[3] + mitgliederIDs[4] + mitgliederIDs[5] + mitgliederIDs[6] + mitgliederIDs[7] + ",timestamp: " + ts;
                            versenden = chatID;
                            for (int i = 0; i < mitgliederIDs.length; i++) {
                                for (ClientHandlerV3 mc : ServerV3.ar) {
                                    if (mc.name.equals(mitgliederIDs[i])) {
                                        mc.dos.writeUTF(versenden);
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    case "loescheChat": // noch nicht fertig
                        System.out.println("<" + currentTimestamp + "> loeascheChat-Anfrage von Client: " + name);
                        this.erfolg = false;
                        if (!this.istLogin) {
                            break;
                        }
                        chatID = splitResult[1];
                        ze_userID = this.name;
                        erfolg = sql.loescheChatErfolg(erfolg, chatID, ze_userID);
                        if (erfolg) {
                            versenden = "User: "+ze_userID+" aus Chat: "+chatID+" rausgegangen"; // an chat und an user einfach erfolg
                        }
                        break;
                    case "addPerson": // noch nicht fertig
                        System.out.println("<" + currentTimestamp + "> addPerson-Anfrage von Client: " + name);
                        erfolg = false;
                        if (!this.istLogin) {
                            System.out.println("nicht eingeloggt");
                            break;
                        }
                        chatID = splitResult[1];
                        za_userID = splitResult[2];
                        ts = splitResult[3] + ";" + splitResult[4] + ";" + splitResult[5] + ";" + splitResult[6] + ";" + splitResult[7];
                        versenden = "addPerson-Anfrage: chatID: " + chatID + ", userID: " + userID + ",timestamp: " + ts;
                        System.out.println(versenden);
                        dos.writeUTF(versenden);
                        break;
                    case "neuerKontakt":
                        System.out.println("<" + currentTimestamp + "> neuerKontakt-Anfrage von Client: " + name);
                        erfolg = false;
                        if (!this.istLogin) {
                            break;
                        }
                        nutzername = splitResult[1];
                        erfolg = sql.neuerKontaktAnfrageErfolg(erfolg, nutzername);
                        if (erfolg) {
                            nK_userID = sql.neuerKontaktAnfrageOutput(nK_userID, nutzername);
                            versenden = nK_userID;
                            for (ClientHandlerV3 mc : ServerV3.ar) {
                                if (mc.name.equals(this.name)) {
                                    mc.dos.writeUTF(versenden);
                                    System.out.println("<" + currentTimestamp + "> " + versenden + " - versendet an:" + name);
                                    break;
                                }
                            }
                            break;
                        }
                        break;
                    case "ausloggen":
                        erfolg = false;
                        if (!this.istLogin) {
                            break;
                        }
                        istOnline = false;
                        this.name = "Client "+ServerV3.clientZaehler+1;
                        istLogin = false;
                        break;
                    case "verbindungBeenden": //wirft wenn nicht gehandelt beim Client fehler aus
                        erfolg = false;
                        System.out.println("<" + currentTimestamp + "> ausloggen-Anfrage von Client: " + name);
                        this.istOnline = false;
                        for (ClientHandlerV3 mc : ServerV3.ar) {
                            if (mc.name.equals(userID)) {
                                ServerV3.ar.remove(mc);
                                if (mc.s == s) {
                                    try {
                                        if (dis != null) {
                                            this.dis.close();
                                            System.out.println("<" + currentTimestamp + "> schließt DataInputStream von Client: "+this.name);
                                        }
                                        if (dos != null) {
                                            this.dos.close();
                                            System.out.println("<" + currentTimestamp + "> schließt DataOutputStream von Client: "+this.name);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    s.close();
                                    if (s == null) {
                                        System.out.println("<" + currentTimestamp + "> Socket von Client: "+this.name+" geschlossen");
                                    }
                                }
                                System.out.println(ServerV3.ar);
                                break;
                            }
                        }
                        bedignung = false;
                        break;
                    default:
                        System.out.println("<" + currentTimestamp + "> unbekannte Anfrage von Client: " + name);
                        break;

                }

            } catch (IOException e) {
                e.printStackTrace();
                break;
            }

        }
        try {
            if (dis != null) {
                this.dis.close();
                System.out.println("<" + currentTimestamp + "> schließt DataInputStream");
            }
            if (dos != null) {
                this.dos.close();
                System.out.println("<" + currentTimestamp + "> schließt DataOutputStream");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

