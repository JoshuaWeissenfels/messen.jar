package Server;

//Imports
import java.io.*;
import java.net.*;
import java.util.*;

/**
 *
 * @author nk.WideView
 */

class ServerV3 {

    //Vektor(synchronisierte Liste) mit allen Clients
    static Vector<ClientHandlerV3> ar = new Vector<>();

    //Anzahl der Clients
    static int clientZaehler = 0;

    //Main Methode des Servers die eine IOExeption produziert aber trotzdem weiter läuft
    public static void main(String[] args) throws IOException {

        //Timestamp für Server-Output
        java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        //ServerSocket wird auf dem Gerät auf Port 8888 gestartet
        ServerSocket ss = new ServerSocket(8888);
        //Eine Verbindung(Socket) s wird festgelegt
        Socket s;
        System.out.println("<"+currentTimestamp+"> "+ss+" gestartet");

        //Während der Server aktiv ist wird die Schleife durchlaufen
        while (true) {
            //eingehende Verbindungen werden akzeptiert
            s = ss.accept();
            System.out.println("<"+currentTimestamp+"> neue Client-Request empfangen: " + s);

            //DataInput- und DataOutputStream der Verbindung werden initialisiert
            DataInputStream dis = new DataInputStream(s.getInputStream());
            DataOutputStream dos = new DataOutputStream(s.getOutputStream());

            //für jede Verbindung wird ein Objekt der Klasse ClientHandlerV3 initialisiert mit den Daten der Verbindung, einem Übergangsnamen mit der Nummer des Clients und den DataInput- und DataOutputStreams
            ClientHandlerV3 ch = new ClientHandlerV3(s,"client " +  clientZaehler, dis, dos);
            System.out.println("<"+currentTimestamp+"> neuer ClientHandler wird für den Client erstellt");

            //Ein Thread aus dem Objekt ClientHandlerV3 wird initialisiert
            Thread t = new Thread(ch);

            //Der ClientHandler der Verbindung wird dem Vektor mit allen Clients hinzugefügt
            ar.add(ch);

            //Der Thread in dem ClientHandler wird gestartet
            t.start();
            System.out.println("<"+currentTimestamp+"> Thread für Client startet");

            System.out.println("<"+currentTimestamp+"> ClientListe:"+ar);
        }
    }

    //Methode um Clients aus der ClientListe zu entfernen
    void chAusArNehmen(ClientHandlerV3 ch) {
        ar.remove(ch);
    }
}