package Server;

import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author nk.WideView
 */

class mysqlVerbindungV3 {

    //SQL-Daten: MessenJarDatenV3 ist der Database Name, wideview ist der Username and lqsymSERVER das Password

    private String driver = "com.mysql.jdbc.Driver";
    private String queue;
    private ArrayList output = new ArrayList();

    boolean loginAnfrageErfolg(boolean erfolg, String userID, String passwordHash) {
        /*
         * Die SQL-Datenbank wird nach einer UserID durchsucht und
         * wenn diese gefunden wurde wird das eingehende Password mit dem,
         * zu der UserID gehörenden, aus der SQL-Datenbank verglichen.
         * Wenn alles gut gelaufen ist, wird erfolg auf true gesetzt und zurückgegeben.
         */
        System.out.println("[SQL]#Login_Anfrage# Daten der Anfrage: Erfolg: "+erfolg+", UserID: "+userID+", PasswordHash: "+passwordHash);
        queue = "SELECT * FROM User WHERE ID = '"+userID+"'";
        sqlOutputQueue(queue);
        System.out.println("[SQL]#Login_Anfrage# Output von Login-Anfrage an SQL-Datenbank/Zeile von User mit angegebener UserID: "+output);
        if(passwordHash.equals(output.get(2))) {
            erfolg = true;
        }
        System.out.println("[SQL]#Login_Anfrage# Erfolg: "+erfolg);
        return erfolg;
    }

    boolean neuerAccountAnfrageErfolg(boolean erfolg, String nutzername, String passwordHash) {
        /*
         * Die SQL-Datenbank wird nach dem Nutzernamen für den neuen Account durchsucht,
         * wenn es diesen dort nicht gibt wird eine UUID als UserID erstellt, die auch noch nicht vergeben ist.
         * Wenn dies geschehen ist wird die freie UUID als UserID zusammen mit dem freien Nutzernamen
         * und dem angegebenen Passwort in die User Tabelle eingetragen.
         * Wenn alles gut gelaufen ist, wird erfolg auf true gesetzt und zurückgegeben.
         */
        System.out.println("[SQL]#neuer_Account_Anfrage# Daten der Anfrage: Erfolg: "+erfolg+", nutzername: "+nutzername+", PasswordHash: "+passwordHash);
        queue = "SELECT * FROM User";
        sqlOutputQueue(queue);
        System.out.println("[SQL]#neuer_Account_Anfrage# Output von neuerAccount-Anfrage an SQL-Datenbank/gesammte User-Tabelle: "+output);
        if (output.contains(nutzername)) {
            System.out.println("[SQL]#neuer_Account_Anfrage# Nutzername von neuerAccount-Anfrage schon in SQL-Datenbank vorhanden");
            erfolg = false;
        } else {
            String uuid = UUID.randomUUID().toString().replace("-","");
            if (output.contains(uuid)) {
                System.out.println("[SQL]#neuer_Account_Anfrage# neue UUID schon in SQL-Database / sollte nicht passieren");
                erfolg = false;
            } else {
                System.out.println("[SQl]#neuer_Account_Anfrage# Daten des neuen Accounts: UserID: "+uuid+" Nutzername: "+nutzername+" passwordHash: "+passwordHash);
                queue = "INSERT INTO User VALUES ('"+uuid+"','"+nutzername+"','"+passwordHash+"')";
                sqlInputQueue(queue);
                queue = "SELECT * FROM User";
                sqlOutputQueue(queue);
                System.out.println("[SQL]#neuer_Account_Anfrage# Output von neuerAccount-Anfrage an SQL-Datenbank/neuer Account sollte vorhanden sein: "+output);
                erfolg = true;
            }
        }
        System.out.println("[SQL]#neuer_Account_Anfrage# Erfolg: "+erfolg);
        return erfolg;
    }

    String neuerAccountAnfrageOutput(String n_UserID, String nutzername) {
        /*
         * Die SQL-Datenbank wird nach dem Nutzernamen des neuen Accounts durchsucht
         * und die dazugehörige neue UserID wird dann ausgelesen und zuückgegeben.
         */
        System.out.println("[SQL]#neuer_Account_Anfrage# Daten der Anfrage: neue UserID. "+n_UserID+", Nutzername: "+nutzername);
        queue = "SELECT * FROM User WHERE Name = '"+nutzername+"'";
        sqlOutputQueue(queue);
        n_UserID = output.get(0).toString();
        System.out.println("[SQL]#neuer_Account_Anfrage# neue UserID: "+n_UserID);
        return n_UserID;
    }

    boolean neueNachrichtAnfrageErfolg(boolean erfolg, String chatID, String senderID, String nachricht, String ts) {
        /*
         * Die SQL-Datenbank wird nach der ChatID des Chats an den die Nachricht gesendet werden soll durchsucht,
         * dann wird überprüft, ob die ChatID existiert und der Sender überhaupt im Chat ist. Wenn das der Fall
         * ist, werden Nachricht, SenderID und Timestamp in die zur ChatID gehörenden Tabelle eingesetzt.
         * Wenn alles gut gelaufen ist, wird erfolg auf true gesetzt und zurückgegeben.
         */
        System.out.println("[SQL]#neue_Nachricht_Anfrage# Daten der Anfrage: Erfolg: "+erfolg+", ChatID: "+chatID+", SenderID: "+senderID+", Nachricht: "+nachricht+", ts: "+ts);
        queue = "SELECT * FROM ChatListe WHERE ChatID = '"+chatID+"'";
        sqlOutputQueue(queue);
        if (output.contains(chatID) && output.contains(senderID)) {
            queue = "SELECT * FROM "+chatID;
            sqlOutputQueue(queue);
            int nachrichtID;
            if (output.isEmpty()) {
                nachrichtID = 1;
            } else {
                nachrichtID = Integer.parseInt(output.get(output.size()-4).toString())+1;
            }
            queue = "INSERT INTO "+chatID+" VALUES ('"+nachrichtID+"','"+nachricht+"','"+senderID+"','"+ts+"')";
            sqlInputQueue(queue);
            erfolg=true;
        }
        return erfolg;
    }

    String[] neueNachrichtAnfrageOutput(String[] mitgliederIDs, String chatID) {
        /*
         * Die SQL-Datenbank wird nach der ChatID des Chats an den die Nachricht gesendet werden soll durchsucht und
         * alle MitgliederIDs die in der Tabelle des Chats vorhanden sind, also die Empfänger der Nachricht, zurückgegeben.
         */
        queue = "SELECT * FROM ChatListe WHERE ChatID = '"+chatID+"'";
        sqlOutputQueue(queue);
        for (int i = 2; i < output.size(); i++) {
            mitgliederIDs[i-2] = output.get(i).toString();
        }
        return mitgliederIDs;
    }

    boolean neuerChatAnfrageErfolg(boolean erfolg, String chatname, String[] mitgliederIDs) {
        //erstellt eine neue Tabelle für den Chat mit dem Namen = ChatID und fügt diese ChatID mit dem Chatnamen und den Chatmitgliedern in die Tabelle ChatListe ein
        System.out.println("[SQL]#neuer_Chat_Anfrage# Daten der Anfrage: Erfolg: "+erfolg+", Chatname: "+chatname+", MitgliederIDs: "+mitgliederIDs);
        queue = "SELECT * FROM ChatListe WHERE Chatname = '"+chatname+"'";
        sqlOutputQueue(queue); //funktioniert
        if (output.contains(chatname)) {
            System.out.println("[SQL]#neuer_Chat_Anfrage# Chatname von neuerChat-Anfrage schon in SQL-Datenbank vorhanden");
            erfolg = false;
        } else {
            for (int i = 0; i < 8; i++) {
                if (mitgliederIDs[i] == null) {
                    mitgliederIDs[i] = "NULL";
                }
            }
            String uuid = UUID.randomUUID().toString().replace("-","");
            queue = "CREATE TABLE "+uuid+" (NachrichtID int(255), Nachricht VARCHAR(255), SenderID VARCHAR(255), Zeitstempel VARCHAR(255))";
            sqlInputQueue(queue);
            queue = "INSERT INTO ChatListe VALUES ('" + uuid + "','" + chatname + "','" + mitgliederIDs[0] + "','" + mitgliederIDs[1] + "','" + mitgliederIDs[2] + "','" + mitgliederIDs[3] + "','" + mitgliederIDs[4] + "','" + mitgliederIDs[5] + "','" + mitgliederIDs[6] + "','" + mitgliederIDs[7] + "')";
            sqlInputQueue(queue);
            queue = "SELECT * FROM ChatListe WHERE ChatID = '"+uuid+"'";
            sqlOutputQueue(queue);
            System.out.println("[SQL]#neuer_Chat_Anfrage# neuer Chat erstellt: ChatID: " + output.get(0) + ", Chatname: " + output.get(1) + ", MitgliederIDs: " + output.get(2) + output.get(3) + output.get(4) + output.get(5) + output.get(6) + output.get(7) + output.get(8) + output.get(9));
            erfolg = true;
        }
        System.out.println("[SQL]#neuer_Chat_Anfrage# Erfolg: "+erfolg);
        return erfolg;
    }

    String neuerChatAnfrageOutput(String chatID, String chatname) {
        //chatID des neuen Chats ausgeben indem man die Tabelle chatliste nach dem char
        queue = "SELECT * FROM ChatListe WHERE Chatname = '"+chatname+"'";
        sqlOutputQueue(queue);
        chatID = output.get(0).toString();
        System.out.println("[SQL]#neuer_Chat_Anfrage# neue ChatID: "+chatID);
        return chatID;
    }

    boolean loescheAccountErfolg(boolean erfolg, String userID) {
        //in tabelle User nach userID suchen dann diese daraus entfernen dann wenn funktioniert hat erfolg auf true setzten und zurückgeben
        return erfolg;
    }

    boolean loescheChatErfolg(boolean erfolg, String chatID, String userID) {
        // in tabelle chatliste  user aus mitglieder raus nehmen und wenn funktioniert erfolg auf true setzen und zurückgeben
        return erfolg;
    }

    boolean neuerKontaktAnfrageErfolg(boolean erfolg, String nutzername) {
        queue = "SELECT * FROM User WHERE Name = '"+nutzername+"'";
        sqlOutputQueue(queue);
        if (output.contains(nutzername)) {
            erfolg = true;
        }
        return erfolg;
    }

    String neuerKontaktAnfrageOutput(String nK_userID, String nutzername) {
        queue = "SELECT * FROM User WHERE Name = '"+nutzername+"'";
        sqlOutputQueue(queue);
        nK_userID = output.get(0).toString();
        return nK_userID;
    }

    void sqlOutputQueue(String outputQueue) {
        try{
            Class.forName(driver);
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/MessenJarDatenV3","wideview","lqsymSERVER");
            System.out.println("[SQL]#output_queue#"+outputQueue+"# erfolgreich mit mysql-Server verbunden");
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery(outputQueue);
            ResultSetMetaData rsmd = rs.getMetaData();
            int spaltenAnzahl = rsmd.getColumnCount();
            output.clear();
            while(rs.next()) {
                for(int i = 0; i<spaltenAnzahl; i++) {
                    output.add(rs.getString(i+1)); //was zur hölle bitte nie wieder es ist so schlimm mit fucking resultsets zu arbeiten
                }
            }
            rs.close();
            con.close();
            System.out.println("[SQL]#output_queue#"+outputQueue+"# Verbindung mit mysql-Server beendet");
        }catch(SQLException se) {
            se.printStackTrace();
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    void sqlInputQueue(String inputQueue) {
        try {
            Class.forName(driver);
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/MessenJarDatenV3","wideview","lqsymSERVER");
            System.out.println("[SQL]#input_queue#"+inputQueue+"# erfolgreich mit mysql-Server verbunden");
            Statement stmt=con.createStatement();
            //ResultSet rs=stmt.executeUpdate(inputQueue); -- nicht noetig
            int i =stmt.executeUpdate(inputQueue);
            con.close();
            System.out.println("[SQL]#input_queue#"+inputQueue+"# Verbindung mit mysql-Server beendet");
        }catch(SQLException se) {
            se.printStackTrace();
        }catch(Exception e) {
            e.printStackTrace();
        }

    }

}