package Client;

import java.io.*;

import java.nio.file.Files;
import java.sql.*;
import java.util.*;



public class DataManager {

    private int userID;

    public DataManager (int currentUser) {
        this.userID = currentUser;
    }

    // neuen Ordner für einen neuen Nutzer erstellen
    public void newUserFolder (int id) {
        String path = "data/" + id;     // pfad, bestehend aus data/ und der ID des neuen Nutzers
        File f = new File(path);        // objekt f von der klasse file

        if (!f.exists()) {              // wenn das objekt im filesystem nicht existiert soll es dort erstellt werden
            f.mkdir();
        }

    }

    public void addPerson (int id, String name, String spitzname) {
        String pfad = "data/" + this.userID + "/Kontakte.txt";      // pfad

        try {

            FileWriter fileWriter = new FileWriter(pfad, true);  // filewriter, der in der Datei mit dem Pfad pfad schreiben kann
                                                                        // append = true bedeutet, dass die datei nicht überschrieben wird,
                                                                        // sondern alles neu am ende hinzugefügt wird

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter); // der sauberkeit halber wird der writer gebuffert

            bufferedWriter.newLine();
            bufferedWriter.write(id + ";" + name + ";" + spitzname);

            bufferedWriter.close();                                     // schliesst die offene datei
        } catch (IOException ex) {
            System.out.println("Error writing to file '" + pfad + "'"); // exception wird abgefangen und fehlermeldung ausgegeben
        }

    }
    public ArrayList<Person> readPerson () {

        ArrayList<Person> person = new ArrayList<Person>();
        int id = 0;
        String name = null;
        String spitzname = null;

        String line = null;

        String file = "data/" + this.userID + "/Kontakte.txt";
        try {

            FileReader fileReader = new FileReader(file);                   // Filereader, der die Datei mit dem Pfad file einlesen kann
            BufferedReader bufferedReader = new BufferedReader(fileReader); // gebuffert

            bufferedReader.readLine(); //  damit die erste zeile ausgelassen wird

            while ((line = bufferedReader.readLine()) != null) {    // die eingelesene Zeile wird dem String line zugewiesen, line = null bedeutet das die zeile leer ist,
                                                                    // dh es wird solange eingelesen bis wir am ende der datei sind

                String[] result = line.split(";");            //der eingelesene String wird aufgeteilt jeweils zwischen den semikola

                id = Integer.parseInt(result[0]);
                name = result[1];
                spitzname = result[2];

                person.add(new Person(id, name, spitzname));        // der liste wird eine neue Person hinzugefügt mit den attributen, die vorher eingelesen wurden

            }

            bufferedReader.close();                                     // schliesst die offene datei
        } catch (FileNotFoundException ex) {

            System.out.println("Unable to open file '" + file + "'");

        } catch (IOException ex) {

            System.out.println("Error reading file '" + file + "'");

        }
        return person;

    }


    public void addChat (int id, String name) { // ec = einzelchat
        String pfad = "data/" + this.userID + "/Kontakte.txt";      // pfad

        try {

            FileWriter fileWriter = new FileWriter(pfad, true);  // filewriter, der in der Datei mit dem Pfad pfad schreiben kann
                                                                        // append = true bedeutet, dass die datei nicht überschrieben wird,
                                                                        // sondern alles neu am ende hinzugefügt wird

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter); // der sauberkeit halber wird der writer gebuffert

            bufferedWriter.newLine();
            bufferedWriter.write(id + ";" + name + ";");

            bufferedWriter.close();                                     // schliesst die offene datei

        } catch (IOException ex) {
            System.out.println("Error writing to file '" + pfad + "'"); // exception wird abgefangen und fehlermeldung ausgegeben
        }

    }
    public ArrayList<Chat> readChats () {
        ArrayList<Chat> chats = new ArrayList<>();

        String file = "data/" + this.userID + "/Chats.txt";
        String line = null;

        int id = 0;
        String name = null;
        boolean ec;

        char semikolon;

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader =
                    new FileReader(file);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader =
                    new BufferedReader(fileReader);

            bufferedReader.readLine(); //  damit die erste zeile ausgelassen wird

            while ((line = bufferedReader.readLine()) != null) {
                semikolon = '0';
                //System.out.println(line);

                id = Integer.parseInt(line.substring(0, 4)); // unter der Annahme, das alle id 4 ziffern lang sind
                //System.out.println(id);


                name = line.substring(5);
                //System.out.println(name);

                chats.add(new Chat(id, name, this.userID));


            }
            // Always close files.
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            file + "'");
        } catch (IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + file + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }
        return chats;
    }


    public void addMessage (GregorianCalendar gc, String inhalt, int chatID, Person absender) {
        String file = "data/" + this.userID + "/" + chatID + ".txt";
        String line = null;
        int id = 1000;
        String stamp = gc.get(Calendar.YEAR) + ";" + (gc.get(Calendar.MONTH) + 1) + ";" + gc.get(Calendar.DAY_OF_MONTH) + ";" + gc.get(Calendar.HOUR) + ";" + gc.get(Calendar.MINUTE );

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader =
                    new FileReader(file);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader =
                    new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {

                int t = Character.getNumericValue(line.charAt(0));
                int h = Character.getNumericValue(line.charAt(1));
                int z = Character.getNumericValue(line.charAt(2));
                int e = Character.getNumericValue(line.charAt(3));

                id = 1000 * t + 100 * h + 10 * z + e;


                //System.out.println(t);
                //System.out.println(h);
                //System.out.println(z);
                //System.out.println(e);
                //System.out.println(id);
            }

            // Always close files.
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            file + "'");
        } catch (IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + file + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        } catch (StringIndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }

        try {
            // Assume default encoding.
            FileWriter fileWriter =
                    new FileWriter(file, true);

            // Always wrap FileWriter in BufferedWriter.
            BufferedWriter bufferedWriter =
                    new BufferedWriter(fileWriter);

            // Note that write() does not automatically
            // append a newline character.
            bufferedWriter.newLine();
            bufferedWriter.write(id + 1 + ";" + stamp + ";" + inhalt + ";" + absender.getPersonId());

            // Always close files.
            bufferedWriter.close();
        } catch (IOException ex) {
            System.out.println("Error writing to file '" + file + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }
    }
    public ArrayList<Nachricht> readMessages (int chatID) {

        ArrayList<Nachricht> messages = new ArrayList<>();

        String file = "data/" + this.userID + "/" + chatID + ".txt";
        String line = null;



        int id;
        GregorianCalendar stamp;
        int[] gc = new int[5];
        String inhalt;
        int absenderID;


        char semikolon;


        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader =
                    new FileReader(file);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader =
                    new BufferedReader(fileReader);

            bufferedReader.readLine(); //  damit die erste zeile ausgelassen wird

            while((line = bufferedReader.readLine()) != null){
                semikolon = '0';

                //System.out.println(line);

                String[] result = line.split(";");

                id = Integer.parseInt(result[0]);

                for (int i = 0; i < 5; i++) {
                    gc[i] = Integer.parseInt(result[i+1]);
                }

                stamp = new GregorianCalendar(gc[0], gc[1], gc[2], gc[3], gc[4]);

                inhalt = result[6];
                absenderID = Integer.parseInt(result[7]);

                messages.add(new Nachricht(id, stamp, inhalt, new Person(absenderID)));

            }
            // Always close files.
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            file + "'");
        }
        catch(IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + file + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }
        return messages;

    }



}
   /* public static void main(String[] args){
        try{

        String user="root";
        String pw="Z53;1d2hjKL9";

        String url="jdbc:mysql://localhost:3306/messengerTest";

        Properties p=new Properties();

        p.setProperty("user",user);
        p.setProperty("password",pw);
        p.setProperty("useSSL","false");
        //p.setProperty("autoReconnect", "true");
        p.setProperty("useLegacyDatetimeCode","false");
        p.setProperty("serverTimezone","UTC");

        Connection con=DriverManager.getConnection(url,p);                           // connection zum server
        Statement sta=con.createStatement();                                          // statement
        ResultSet res=sta.executeQuery("select * from user order by id asc");    // sql befehl

        while(res.next()){ // .next geht zur nächsten zeile des ergebnisses und returnt false wenn es keine zeilen mehr gibt
        System.out.println(res.getInt("id")+" is the id of "+res.getString("name"));

        }

        }catch(Exception exc){
        exc.printStackTrace();
        }finally{
        //System.out.println("tja");
        }
        }

        public static void main(String [] args) {

        // The name of the file to open.
        String file = "data/Kontakte.txt";

        // This will reference one line at a time
        String line = null;

        try {
            // Assume default encoding.
            FileWriter fileWriter =
                    new FileWriter(file, true);

            // Always wrap FileWriter in BufferedWriter.
            BufferedWriter bufferedWriter =
                    new BufferedWriter(fileWriter);

            // Note that write() does not automatically
            // append a newline character.
            bufferedWriter.write("Hello there,");
            bufferedWriter.write(" here is some text.");
            bufferedWriter.newLine();
            bufferedWriter.write("We are writing");
            bufferedWriter.write(" the text to the file.");

            // Always close files.
            bufferedWriter.close();
        }
        catch(IOException ex) {
            System.out.println(
                    "Error writing to file '"
                            + file + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }
        System.out.println("writer finished");
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader =
                    new FileReader(file);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader =
                    new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }

            // Always close files.
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            file + "'");
        }
        catch(IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + file + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }
    }*/