package Client;

import java.io.*;

public class Person {


  // Anfang Attribute
    private int personId;
    private String name;
    private String spitzname;
  // Ende Attribute

    public Person (int newID) {
        this.personId = newID;

    }

    public Person (int newID, String newName) { // evtl weglasssen
        this.personId = newID;
        this.name = newName;
    }

    public Person (int newID, String newName, String neuerSpitzname) {
        this.personId = newID;
        this.name = newName;
        this.spitzname = neuerSpitzname;
    }
  // Anfang Methoden


    public int getPersonId () {
        return personId;
    }

    public String getName() {
        return this.name;
    }

    public String getSpitzname() {
        return spitzname;
    }

    public void setName(String newName) {
        this.name = newName;
    }
  // Ende Methoden

}
