package Client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;

public class Sort {
    private Chat[] arr;

    public Sort(ArrayList<Chat> chats){
        this.arr = chats.toArray(new Chat[0]);

    }

    public void chatsTauschen(int stelle1, int stelle2){                         // zahlen tauschen
        Chat temp = this.arr[stelle1];
        this.arr[stelle1] = this.arr[stelle2];
        this.arr[stelle2] = temp;
    }

    public void quickSort(int lowerIndex, int higherIndex) {                      // rekursiver sort
        int i = lowerIndex;
        int j = higherIndex;
        GregorianCalendar pivot = this.arr[(lowerIndex + higherIndex)/2].getLatest();//getNachricht().get(this.arr[(lowerIndex + higherIndex)/2].getNachricht().size() - 1).getGc();                         // berechnung des Pivotelements



        while (i <= j) {                                                            /* mit jeder wiederholung wird ein element, dass gr��er als das pivotelement ist,
                                                                                   mit einem Element, dass kleiner als das Pivotelement ist, getauscht */

            while (this.arr[i].getLatest().before(pivot)) {                                             // wird wiederholt bis ein Element gefunden wird, welches gr��er als das Pivotelement ist
                i++;
            }

            while (this.arr[j].getLatest().after(pivot)) {                                             // wird wiederholt bis ein Element gefunden wird, welches kleiner als das Pivotelement ist
                j--;
            }

            if (i <= j) {                                                             // wird nur getauscht wenn das kleinere element an einer fr�heren oder gleichen Stelle steht
                chatsTauschen(i, j);
                i++;
                j--;
            }
        }

        if (lowerIndex < j){                                                        // Selbstaufruf f�r den unteren Teil der Liste
            quickSort(lowerIndex, j);
        }
        if (i < higherIndex){                                                       // Selbstaufruf f�r den oberen Teil der Liste
            quickSort(i, higherIndex);
        }
    }

    public Chat[] getArr () {
        return arr;
    }

    public ArrayList<Chat> getList () {
        return new ArrayList<Chat>(Arrays.asList(arr));
    }
}
