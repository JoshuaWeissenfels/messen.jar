package Client;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.CountDownLatch;

public class ServerManager {

    private int ServerPort;
    private InetAddress ip;
    private Socket s;
    Scanner scn;
    private DataInputStream dis;
    private DataOutputStream dos;

    public ServerManager () {

        try {
            this.scn = new Scanner(System.in);
            this.ServerPort = 8888;
            this.ip = InetAddress.getByName("84.140.159.160");
            this.s = new Socket(ip, ServerPort);
            this.dis = new DataInputStream(this.s.getInputStream());
            this.dos = new DataOutputStream(this.s.getOutputStream());
        }catch(Exception ex){
            ex.printStackTrace();
        }

    }

    public static void main(String args[]) throws UnknownHostException, IOException
    {

        ServerManager sm = new ServerManager();

        // obtaining input and out streams


        // sendMessage thread
        /*Thread sendMessage = new Thread(new MyRunnable(sm.dis, sm.dos, "platzhalter")
        {
            @Override
            public void run() {
                while (true) {

                    // read the message to deliver.
                    String msg; //= "Test";
                    msg = this.scn.nextLine();

                    try {
                        // write on the output stream
                        this.dos.writeUTF(msg);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        // read the message sent to this client
                        msg = this.dis.readUTF();
                        System.out.println(msg);
                    } catch (IOException e) {

                        e.printStackTrace();
                    }
                }
            }
        });

        sendMessage.start();*/
    }

    /*public boolean login (int userID, String pwHash){

        Thread sendMessage = new Thread(new Runnable()
        {
            @Override
            public void run() {
                while (true) {
                    Scanner scn = scanner;
                    // read the message to deliver.
                    String msg; //= "Test";
                    msg = scn.nextLine();

                    try {
                        // write on the output stream
                        dos.writeUTF(msg);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        return false;
    }*/

    public String getName (int userID) { //erwartet
        String name = "name";
        return name;
    }

    public String neueNachricht (int chatID, Nachricht message) {
        String msg = "neueNachricht;" + chatID + ";" + message.getInhalt() + ";" + message.getTimestamp();
        String returned = null;


        try {
            // write on the output stream
            this.dos.writeUTF(msg);
            System.out.println("sent");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            // read the message sent to this client
            returned = this.dis.readUTF();
            System.out.println("received");
            System.out.println(msg);
        } catch (IOException e) {

            e.printStackTrace();
        }

        // sendMessage thread
        //Thread sendMessage = new Thread(new MyRunnable(this.dis, this.dos, msg)
        /*{
            @Override
            public void run() {
                try {
                    // write on the output stream
                    this.dos.writeUTF(this.msg);
                    System.out.println("sent");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    // read the message sent to this client
                    this.msg = this.dis.readUTF();
                    System.out.println("received");
                    System.out.println(this.msg);
                } catch (IOException e) {

                    e.printStackTrace();
                }

            }
        });*/

        //sendMessage.start();

        return returned;
    }

    public boolean newChat (ArrayList<Person> p){
        return false;
    }

    public boolean newUser (String name, String pwHash){
        return false;
    }

    public boolean addPersonToChat (int chatID, int userID){
        return false;
    }

    public String login (int nutzerID, String pwHash){

        String msg = "Login;" + nutzerID + ";" + pwHash;
        String returned = null;

        try {
            // write on the output stream
            this.dos.writeUTF(msg);
            System.out.println("sent");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            // read the message sent to this client
            returned = this.dis.readUTF();
            System.out.println("received");
            System.out.println(returned);
        } catch (IOException e) {

            e.printStackTrace();

        }


        /*final CountDownLatch latch = new CountDownLatch(1);
        String returned;

        Runnable rn = new MyRunnable(this.dis, this.dos, msg);

        // sendMessage thread
        Thread sendMessage = new Thread(rn);

        sendMessage.join();
        try {
            sendMessage.start();
            latch.await();
            return
        }catch(Exception ex){
            ex.printStackTrace();
        }

        /*Thread getMessage = new Thread((new Runnable() {
            @Override
            public void run () {

            }
        }));*/

        return returned;
    }

    public void ausloggen (){
        String msg  = "ausloggen";

        try {
            // write on the output stream
            this.dos.writeUTF(msg);
            System.out.println("sent");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
// Typ;