package Client;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Chat {

    public Chat (int newId, int userID) {
        this.name = "new Chat";
        this.chatID = newId;
        this.nachricht = new ArrayList<>();
        dm = new DataManager(userID);

    }
    public Chat (int newId, String newName, int userID) {
        this.name = newName;
        this.chatID = newId;
        this.nachricht = new ArrayList<>();
        dm = new DataManager(userID);

    }

    private DataManager dm;

    private String name;
    private int chatID;
    private ArrayList<Nachricht> nachricht;
    private ArrayList<Person> mitglied;
    private GregorianCalendar latest;

    public void addNachricht(String inhalt,Person absender){
        this.nachricht.add(new Nachricht(inhalt, absender));
        this.dm.addMessage(this.nachricht.get(this.nachricht.size() - 1).getGc(), this.nachricht.get(this.nachricht.size() - 1).getInhalt(), this.chatID, this.nachricht.get(this.nachricht.size() - 1).getAbsender());
        this.latest = new GregorianCalendar();
    }
    public void loadMessages(){
        this.nachricht = this.dm.readMessages(chatID);
        this.latest = this.nachricht.get(this.nachricht.size() - 1).getGc();
    }
    public void printMessages () {
        for (int i = 0; i < nachricht.size(); i++) {
            System.out.println(this.nachricht.get(i).getInhalt());
        }

    }


    public DataManager getDm () {
        return dm;
    }
    public int getId () {
        return chatID;
    }
    public String getName () {
        return name;
    }
    public ArrayList<Nachricht> getNachricht () {
        return nachricht;
    }
    public ArrayList<Person> getMitglied () {
        return mitglied;
    }
    public GregorianCalendar getLatest () {
        return latest;
    }

    public void setName (String name) {
        this.name = name;
    }

}
