package Client;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Nachricht {

    private int id;
    private GregorianCalendar gc;
    private String inhalt;
    private Person absender;


    public Nachricht (String inhalt, Person p) { // erstellt nachricht mit aktuellem zeitstempel
        this.inhalt = inhalt;
        this.gc = new GregorianCalendar();
        this.absender = p;
    }

    public Nachricht (int id, GregorianCalendar gc, String inhalt, Person p) { // ertellt nachricht mit übergebenem Zeitstempel
        this.inhalt = inhalt;
        this.gc = gc;
        this.id = id;
        this.absender = p;
    }

    public Person getAbsender () {
        return absender;
    }

    public String getTimestamp () {
        return (gc.get(Calendar.YEAR) + ";" + gc.get(Calendar.MONTH) + ";" + gc.get(Calendar.DAY_OF_MONTH) + ";" + gc.get(Calendar.HOUR) + ";" + gc.get(Calendar.MINUTE));
    }
    public GregorianCalendar getGc () {
        return gc;
    }
    public String getInhalt () {
        return inhalt;
    }
    public int getId () {
        return id;
    }
}
