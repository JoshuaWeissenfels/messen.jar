package Client;

import javax.swing.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class ChatManager {

    ArrayList<Person> person;   // kontakte
    ArrayList<Chat> chats;      // chats
    private DataManager dm;     // verbindung zum lokalen speicher
    private int currentUser;    // aktueller nutzer


    public ChatManager (int currentUser, boolean load) {
        this.dm = new DataManager(currentUser);
        this.currentUser = currentUser;
        this.person = new ArrayList<>();
        this.chats = new ArrayList<>();

        if(load) {                  // es kann entschieden werden ob daten aus dem lokalen speicher geladen werden ( zu Testzwecken )
            this.loadChat();
            this.loadPerson();
        }

    }

        // kontakte aus dem lokalen speicher laden
    public void loadPerson () {

        this.person = dm.readPerson();
    }
        // kontakte hinzufügen
    public void addPerson (int id, String name, String spitzname) {

        this.person.add(new Person(id, name, spitzname));       // zur Liste hinzufügen
        this.dm.addPerson(id, name, spitzname);                 // lokal speichern

    }
        // ausgabe in der konsole ( zu Testzwecken )
    public void printPerson (){
        for (Person i : person) {
            System.out.println(i.getPersonId() + ";" + i.getName() + ";" + i.getSpitzname());
        }

    }

        // chats aus dem lokalen speicher laden
    public void loadChat () {

        this.chats = dm.readChats();        // chats aus dem speicher in die Liste laden
        for (Chat c : this.chats) {         // für jeden chat die nachrichten aus dem speicher in die jeweilige liste laden
            c.loadMessages();
        }

    }
        // chat hinzufügen
    public void addChat (int id, String name) {

        this.chats.add( new Chat(id, name, this.currentUser));      // zur liste hinzufügen
        this.dm.addChat(id, name);                                  // lokal speichern

    }
        // ausgabe in der konsole ( zu Testzwecken )
    public void printChat (){
        for (Chat c : this.chats) {
            System.out.println(c.getId() + ";" + c.getName());
        }
    }


   /* public void newPersonBtn () {
        String name = "";
        String eingabe = "";
        int id = 1;
        boolean neu = false;

        try {
            do{

                eingabe = JOptionPane.showInputDialog("User ID:"); // eigene pop up methode schreiben in gui
                System.out.println(eingabe);

                id = Integer.parseInt(eingabe);

                name = "Lennard";   // eigentlich server nach namen zur id fragen


            } while (name == "-1");
                                    // + abfrage ob ... wirklich hinzugefügt werden soll
            for (int i = 0; i < this.person.size(); i++) {
                if(this.person.get(i).getPersonId() == id) {neu = true;}
                                    // ausgabe das kontakt schon existiert
            }
            if(neu) {
                this.person.add(new Person(id, name));
                System.out.println(this.person.get(this.person.size() -1).getName() + " wurde zu deinen Kontakten hinzugefügt");
            }


        } catch (NumberFormatException e) {
            String s = e.getMessage();
            System.out.println("wurde abgebrochen");
            System.out.println(s);

        } finally {
            //System.out.println("unbekannter fehler");
        }
    }*/

   // public void bootProcedure() {
        //hier müssen alle inhalte vom internen speicher oder server in die Listen geladen werden.
   // }
}


    /*public static void main (String[] args) {
        Person andre = new Person(1234, "André");
        EinzelChat chat1 = new EinzelChat(1, andre.getName());
        System.out.println(chat1.getName());
        chat1.setName("lala");
        System.out.println(chat1.getName());*/ /*public Person getPersonByID (int id){
        for (int i = 0; i < person.size(); i++) {
            if(person.get(i).getPersonId() == id){
                return person.get(i);
            }
        }
        return new Person(id);
    } // returns the person with given id // if there is no such person it returns an empty person with given id*/